# Update, Upgrade, and Clean your Debian-based system automatically at every boot.

These are simple script and service file when placed in proper place can update, upgrade and clean your syystem at every boot.
And it creates a separate log file too.

---

INSTALLATION

```
git clone https://gitlab.com/bx2nero/auto-update-startup-debian.git
cd auto-update-startup-debian
chmod +x install.sh && ./install.sh

```

That's it !!
Now everytime you boot-up your system it will automatically update, upgrade and clean your system.
And you will see a file in your Documents folder named "ulog" where you can see what updates were installed in the last boot.
Enjoy !!.
