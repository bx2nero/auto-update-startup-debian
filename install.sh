#!/bin/bash

echo -e "\e[1;32m\nINSTALLING...\e[0m\n"
echo -n "}> /home/$USER/Documents/update.log" >> updater.sh
sudo cp updater.sh /usr/local/bin/
sudo cp updater.service /etc/systemd/system/
sudo chmod 744 /usr/local/bin/updater.sh
sudo chmod 664 /etc/systemd/system/updater.service
sudo systemctl daemon-reload
sudo systemctl enable updater.service
echo -e "\e[1;32m\nINSTALLATION COMPLETED !!!\e[0m\n"
echo -e "\e[1;32m\nYou can find update log in your Documents folder\e[0m\n"
